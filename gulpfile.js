var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var proxyMiddleware = require('http-proxy-middleware');
var conf = require('./conf');

gulp.task('serve', function() {
  browserSync.init({
    server: {
      baseDir: '/',
      middleware: [
        proxyMiddleware('/api', {
          target: conf.options.api,
          changeOrigin: true
        }),
        proxyMiddleware('/', {
          target: conf.options.web,
          changeOrigin: true
        })
      ]
    },
    ghostMode: false,
    reloadOnRestart: false,
    notify: false
  });
});

gulp.task('default', function() {
  gulp.start('serve');
});
